# README #

Setup:

you will need ssh access to dev4-s4m, pipeline-s4m and goldenorb. 

you will need to be in the portaladmin group to access the datasets.


Run:

./convert_to_pca_interactive.sh <ds_id> <data_type>

where ds_id is the dataset id and data_type is either l,m or r

r is for RNASeq

m is for microarray

l is for local microarray (shouldn't be used anymore?)