#!/bin/bash
#code to take from goldenorb and put into dev4 for goldenorb RNASeq
#code to take local microarrray given to me by Tyrone and send it to dev4

ds_id="$1"
data_type="$2" # m for Microarray, r for RNASeq, l for local microarray

if [ $# -eq 0 ]; then
    echo "have to provide a ds_id "
    exit 1
fi

if [ -z ${data_type} ]; then 
    data_type="r" # for RNASeq
fi
mkdir /tmp/pca/
mkdir /tmp/pca/${ds_id}



if [ "$data_type" = "r" ]; then

    main_qc_dir=/stemformatics/data/datasets/${ds_id}/source/processed/qc
    targets_dir=/stemformatics/data/datasets/${ds_id}/source/raw

    scp -P 2023 orbrmosbe@goldenorb.aibn.uq.edu.au:/${targets_dir}/targets.txt /tmp/pca/${ds_id}


    for name in CPM_log2 RPKM_log2 non_normalized TMM_RPKM_log2
    do

        scp -P 2023 orbrmosbe@goldenorb.aibn.uq.edu.au:/${main_qc_dir}/PCA_gene_expression_${name}_sample_data.tsv /tmp/pca/${ds_id}
        scp -P 2023 orbrmosbe@goldenorb.aibn.uq.edu.au:/${main_qc_dir}/PCA_gene_expression_${name}_screetable_prcomp_variance.tsv /tmp/pca/${ds_id}


        cd /tmp/pca/${ds_id}

        cp  PCA_gene_expression_${name}_sample_data.tsv ${ds_id}_${name}_pca.tsv
        cp  PCA_gene_expression_${name}_screetable_prcomp_variance.tsv ${ds_id}_${name}_screetable.tsv
        cp  targets.txt ${ds_id}_${name}_metadata.tsv

        if `head -1 ${ds_id}_${name}_pca.tsv | grep -q SampleID`; then
            echo "SampleID found. Make no changes."
        else
            sed -i ' 1 s/^/SampleID\t/' ${ds_id}_${name}_pca.tsv
        fi


        var="prcomp\teigenvalue"
        sed -i "1s/.*/$var/" ${ds_id}_${name}_screetable.tsv

        ssh dev4.stemformatics.org "rm -fR /var/www/html/data/${ds_id}_${name}"
        ssh dev4.stemformatics.org "mkdir /var/www/html/data/${ds_id}_${name}"
        scp ${ds_id}_${name}* dev4.stemformatics.org:/var/www/html/data/${ds_id}_${name}/

    done;
fi


if [ "$data_type" = "l" ]; then
    # for microarrays
    cp postnorm_pca_sampleids_sample_data.tsv ${ds_id}_pca.tsv
    cp postnorm_pca_sampleids_screetable_prcomp_variance.tsv ${ds_id}_screetable.tsv
    cp targets.txt ${ds_id}_metadata.tsv
    var="prcomp\teigenvalue"
    sed -i "1s/.*/$var/" ${ds_id}_screetable.tsv


    ssh dev4.stemformatics.org "rm -fR /var/www/html/data/${ds_id}"
    ssh dev4.stemformatics.org "mkdir /var/www/html/data/${ds_id}"
    scp ${ds_id}* dev4.stemformatics.org:/var/www/html/data/${ds_id}/



fi



if [ "$data_type" == "m" ]; then

    cd /tmp/pca/${ds_id}

    main_qc_dir=/data/datasets/${ds_id}/source/normalized

    for file in `ssh pipeline.stemformatics.org "ls /data/datasets/${ds_id}/sample_pheno.* | grep -v sorted"` 
    do
        scp pipeline.stemformatics.org:${file} /tmp/pca/${ds_id}/
    done

    cat /tmp/pca/${ds_id}/sample_pheno.txt | sort > /tmp/pca/${ds_id}/${ds_id}_metadata.tsv

    header="SampleID\tSampleType"
    for file in `ls /tmp/pca/${ds_id}/sample_pheno.txt.* | grep -v template | grep -v sorted` 
    do

        header="${header}\t${file##*.}"

        sort $file > ${file}.sorted
        join -1 1 -2 1 /tmp/pca/${ds_id}/${ds_id}_metadata.tsv ${file}.sorted -t $'\t' > /tmp/pca/${ds_id}/${ds_id}_metadata.tsv.1
        cat /tmp/pca/${ds_id}/${ds_id}_metadata.tsv.1 > /tmp/pca/${ds_id}/${ds_id}_metadata.tsv

    done;

    sed -i "1s/^/${header}\n/" /tmp/pca/${ds_id}/${ds_id}_metadata.tsv

    for norm_type in prenorm_pca postnorm_pca
    do
        for name in nolabels
        do
            scp pipeline.stemformatics.org:/${main_qc_dir}/${norm_type}_${name}_sample_data.tsv /tmp/pca/${ds_id}/${ds_id}_${norm_type}_pca.tsv
            scp pipeline.stemformatics.org:/${main_qc_dir}/${norm_type}_${name}_screetable_prcomp_variance.tsv /tmp/pca/${ds_id}/${ds_id}_${norm_type}_screetable.tsv


            var="prcomp\teigenvalue"
            sed -i "1s/.*/$var/" ${ds_id}_${norm_type}_screetable.tsv


            ssh dev4.stemformatics.org "rm -fR /var/www/html/data/${ds_id}_${norm_type}"
            ssh dev4.stemformatics.org "mkdir /var/www/html/data/${ds_id}_${norm_type}"
            scp /tmp/pca/${ds_id}/${ds_id}_${norm_type}* dev4.stemformatics.org:/var/www/html/data/${ds_id}_${norm_type}/
            scp /tmp/pca/${ds_id}/${ds_id}_metadata.tsv dev4.stemformatics.org:/var/www/html/data/${ds_id}_${norm_type}/${ds_id}_${norm_type}_metadata.tsv
        done;
    done;


    #rm -fR /tmp/pca/* 
fi

